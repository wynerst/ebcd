ALTER TABLE `nodes`
ADD `cp_name` VARCHAR(30) NULL AFTER `post_code`, 
ADD `cp_phone` VARCHAR(30) NULL AFTER `cp_name`,  
ADD `level` VARCHAR(10) NULL AFTER `cp_phone`, 
ADD `status` VARCHAR(10) NULL AFTER `level`, 
ADD `province` VARCHAR(70) NULL AFTER `status`, 
ADD `city` VARCHAR(70) NULL AFTER `province`, 
ADD `district` VARCHAR(70) NULL AFTER `city`,
ADD `address` TEXT NULL AFTER `district` ;