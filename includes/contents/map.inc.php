<?php
/**
 * Copyright (C) 2007,2008  Arie Nugraha (dicarve@yahoo.com), Hendro Wicaksono (hendrowicaksono@yahoo.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

// be sure that this file not accessed directly
if (!defined('INDEX_AUTH')) {
    die("can not access this file directly");
} elseif (INDEX_AUTH != 1) { 
    die("can not access this file directly");
}

$page_title = 'maps';
$info = 'maps';
ob_start();
?>
<div class="ucs-page ucs-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3 class="mt-2 font-weight-bold">Peta Sebaran</h3>                    
            </div>
        </div>
    </div>
</div>

            <?php 
            $node = json_decode($node_location);
            $a = array();
            foreach ($node as $key => $value) {
              if($value[1]!='' || $value[2]!=''){
              $a[] = array('lat'=>$value[1],'lng'=>$value[2],'title'=>'\''.str_replace('</b>','',str_replace('<b>','',str_replace('<br/>',"\n",$value[3]))).'\'');
            }
            }

             $node_s = str_replace('"','',json_encode($a));
             ?>


    <div id="map" style="height:500px;"></div>
    <br/><hr/>
         <h5 style="text-align:center;">Jumlah sekolah tergabung : <?php echo count(json_decode($node_location))?></h5>
    <script>
      var test = <?php echo $node_s; ?>;

      function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 5,
          center: {lat: -2.0097465, lng: 117.829518}

        });
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var markers = locations.map(function(location, i) {
          setAksi(map);
          return new google.maps.Marker({
            position: location,
            //label: 'test',
            //icon: './themes/default/images/logo.png', 
            title: location['title'],
 
            zIndex: 1, 
            html: 'ghgf'    
          });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }
      var locations = test

      function setAksi(map){           
          google.maps.event.addListener(map, "mouseover", function(event) {  
           var lat = event.latLng.lat();  
           var lng = event.latLng.lng();            
           google.maps.event.addListener(marker, "mouseover", function() {                  
              infowindow.setContent(this.html);  
              infowindow.open(map, this);  
            });             
        });  
      } 

    </script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBZY7bxlwBh14vQZh3FogMPZcInxT6tgc&callback=initMap"></script>
<?php 
echo ob_get_clean(); 
echo '<br />'."\n";
?>
