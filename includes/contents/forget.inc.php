<?php
$info = 'Resend UCS node';
if(isset($_POST['resend'])){

	$msg      = '';
    $npsn     = trim($dbs->escape_string(strip_tags($_POST['npsn'])));
    $email    = trim($dbs->escape_string(strip_tags($_POST['email'])));
    $reset    = isset($_POST['reset'])?trim($dbs->escape_string(strip_tags($_POST['reset']))):'';
    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $sysconf['recaptcha']['secret_key'] . "&response=" . $_POST['g-recaptcha-response'] . "&remoteip=" . $sysconf['recaptcha']['ip_addr']);
    $responseKeys = json_decode($response, true);
    
    if (intval($responseKeys["success"]) !== 1) {
        $msg .= 'invalid token';
    }
    
    else{

        require SIMBIO_BASE_DIR .'simbio_DB/simbio_dbop.inc.php';
        require LIB_DIR . 'mail_send.inc.php';

        $npsn_q     = $dbs->query("SELECT * FROM nodes WHERE node_id='" . $npsn . "' AND cp_email='" . $email . "' LIMIT 0,1");
        $npsn_count = $npsn_q->num_rows;

        if ($npsn_count > 0) {
        	$data = $npsn_q->fetch_assoc();
        	if($reset == 'true'){
                $sql_op    = new simbio_dbop($dbs);
        		$data_update['last_update'] = date("Y-m-d H:i:s");
        		$data_update['password'] = sha1(rand());
        		$update = $sql_op->update('nodes', $data_update,'node_id='.$npsn); 
        		if($update){
        			$subject = 'Set Ulang Password UCS Kamaya';
                    $content  = 'Yth. <b>'.$data['cp_name'].'</b>, anda telah mengajukan set ulang password kode simpul Kamaya.'."<br/>";
                    $content .= 'Berikut data konfigurasi terbaru simpul UCS untuk '.$data['name']."<br/>";
                    $content .= 'Node ID : '.$data['node_id'] ."<br/>";
                    $content .= 'Password : '.$data_update['password']."<br/>";
                    $content .= 'Alamat peladen : http://psbsekolah.kemdikbud.go.id/kamaya/'."<br/>";
        		} 
        	}
        	else{
        		$subject = 'Kirim Ulang Password UCS Kamaya';
                $content  = 'Yth. <b>'.$data['cp_name'].'</b>, anda telah melakukan permohonan pengiriman kode simpul Kamaya.'."<br/>";
                $content .= 'Berikut data konfigurasi simpul UCS untuk '.$data['name']."<br/>";
                $content .= 'Node ID : '.$data['node_id'] ."<br/>";
                $content .= 'Password : '.$data['password'] ."<br/>";
                $content .= 'Alamat peladen : http://psbsekolah.kemdikbud.go.id/kamaya/'."<br/>";      		
        	}

            $mail = new email();
            $email = $mail->sendTo($email,$data['cp_name']);
            $email = $mail->setTemplate(ADMIN_THEMES_BASE_DIR.'mail_template.tpl.php');
            $email = $mail->addSubject($subject);
            $email = $mail->addContent($content);
            $email = $mail->send();
            if($email == true){
            	$msg .= 'email berhasil dikirim';
            }else{
            	$msg .= 'email gagal terkirim';
            }
        }

        //if not registered
        else{
        	$msg .= 'maaf, node atau email tidak terdaftar';
        }
    
    }

echo '<div class="alert alert-info">'.$msg.'</div>';
	
}
?>

	<form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		  <div class="form-group">
		    <label class="control-label col-sm-2" for="email">Email</label>
		    <div class="col-sm-10">
		      <input type="email" class="form-control" id="email" name="email" required>
		    </div>
		  </div>

		  <div class="form-group">
		    <label class="control-label col-sm-2" for="pwd">NPSN</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="text" name="npsn" required>
		    </div>
		  </div>  

		  <div class="form-group" title="jika anda memilih opsi ini, ">
		    <label class="control-label col-sm-6" for="pwd"><input type="checkbox" name="reset" value="true"> Reset Password Node</label>

		  </div>

		  <div class="form-group row">
		    <div class="col-sm-10">
		        <div class="g-recaptcha" data-sitekey="<?php echo $sysconf['recaptcha']['site_key'];?>"></div>
		    </div>
			<div class="col-sm-2  pull-right">
			  	<input type="submit" class="btn btn-warning pull-right" value="Kirim Ulang" name="resend"/>
			</div>
		  </div> 
	</form>
<script src='https://www.google.com/recaptcha/api.js'></script>  