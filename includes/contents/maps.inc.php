<?php
/**
 * Copyright (C) 2007,2008  Arie Nugraha (dicarve@yahoo.com), Hendro Wicaksono (hendrowicaksono@yahoo.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

// be sure that this file not accessed directly
if (!defined('INDEX_AUTH')) {
    die("can not access this file directly");
} elseif (INDEX_AUTH != 1) { 
    die("can not access this file directly");
}

$page_title = 'maps';
$info = 'maps';
ob_start();
?>
<div class="ucs-page ucs-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3 class="mt-2 font-weight-bold">Peta Sebaran</h3>                    
            </div>
        </div>
    </div>
</div>

<div class="ucs-page ucs-page-alt">
    <div class="container">
  		<div class="panel-body">
  		    <div class="well" id="map_canvas" style="min-height:500px !important;"></div>            
  		</div>
            <hr/>
      <h5 style="text-align:center;">Jumlah sekolah tergabung : <?php echo count(json_decode($node_location))?></h5>
	</div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBZY7bxlwBh14vQZh3FogMPZcInxT6tgc" type="text/javascript"></script>
<script type="text/javascript"> 
  (function() {  
    var infowindow = null;     
        initialize(); 
  })();   
      function initialize() {   
          var sites = <?php echo $node_location; ?>  
          var centerMap = new google.maps.LatLng(-2.0097465,117.829518);         
          var myOptions = {  
            zoom: 5,  
            center: centerMap,  
            mapTypeId: google.maps.MapTypeId.ROADMAP 
            }    
          var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);    
             setMarkers(map, sites); 
             setAction(map); 
             infowindow = new google.maps.InfoWindow({  
                content: "loading..."  
              });  
          var bikeLayer = new google.maps.BicyclingLayer();  
          bikeLayer.setMap(map);       
      }   
      function setMarkers(map, markers) {  
        for (var i = 0; i < markers.length; i++) {  
         var sites = markers[i];  
         var siteLatLng = new google.maps.LatLng(sites[1], sites[2]);  
         var marker = new google.maps.Marker({  
            position: siteLatLng,  
            map: map,  
            title: sites[0],  
            zIndex: 1, 
            //icon: './themes/default/images/logo.png', 
            html: sites[3]     
            });     
          google.maps.event.addListener(marker, "mouseover", function () {             
            infowindow.setContent(this.html);  
            infowindow.open(map, this);  
          });  
          google.maps.event.addListener(marker, "click", function () {             
            infowindow.setContent(this.html);  
            infowindow.open(map, this);  
          });  
        }  
      }  
      function setAction(map){           
          google.maps.event.addListener(map, "mouseover", function(event) {  
           var lat = event.latLng.lat();  
           var lng = event.latLng.lng();            
           google.maps.event.addListener(marker, "mouseover", function() {                  
              infowindow.setContent(this.html);  
              infowindow.open(map, this);  
            });             
        });  
      } 
 </script>

<?php 
echo ob_get_clean(); 
echo '<br />'."\n";
?>
