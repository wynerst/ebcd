<?php
/**
 * ucs node registration
 * 
 *
 * Copyright (C) 2009 Arie Nugraha (dicarve@yahoo.com), 2018 Heru Subekti (heroe.soebekti@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

// be sure that this file not accessed directly
if (!defined('INDEX_AUTH')) {
    die("can not access this file directly");
} elseif (INDEX_AUTH != 1) {
    die("can not access this file directly");
}

$page_title = __('Registration');
$info       = __('UCS Node Registration');

if (isset($_POST['register'])) {
    echo '<div class="alert alert-info">';
    $msg          = '';
    $response     = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $sysconf['recaptcha']['secret_key'] . "&response=" . $_POST['g-recaptcha-response'] . "&remoteip=" . $sysconf['recaptcha']['ip_addr']);
    $responseKeys = json_decode($response, true);
    
    if (intval($responseKeys["success"]) !== 1) {
        $msg .= __('invalid token');
    }
    
    else {
        
        $unique_code = trim($dbs->escape_string(strip_tags($_POST['unique_code'])));
        $npsn        = trim($dbs->escape_string(strip_tags($_POST['npsn'])));
        $email       = trim($dbs->escape_string(strip_tags($_POST['email'])));
        $cp_name     = trim($dbs->escape_string(strip_tags($_POST['operator_name'])));
        $url         = trim($dbs->escape_string(strip_tags($_POST['baseurl'])));
        
        $npsn_q     = $dbs->query("SELECT node_id FROM nodes WHERE node_id='" . $npsn . "' or cp_email='" . $email . "'");
        $npsn_count = $npsn_q->num_rows;
        
        if (empty($unique_code) && empty($npsn)) {
            $msg .= __('NPSN and / or unique code can not empty');
        }
        
        elseif ($npsn_count > 0) {
            $msg .= __('email or node has been registered');
        }
        
        else {

            require LIB_DIR .'npsn_request.inc.php';
            require SIMBIO_BASE_DIR .'simbio_DB/simbio_dbop.inc.php';
            require LIB_DIR . 'mail_send.inc.php';      
   
            $_npsn = new npsn();
            $_npsn->getnpsn($npsn);
            $data_npsn= $_npsn->result();
 
            if($data_npsn!=false){
                $data['node_id']           = $npsn;
                $data['name']              = $data_npsn->nama;
                $data['address']           = $data_npsn->alamat;
                $data['status']            = $data_npsn->status;
                $data['latitude']          = $data_npsn->latitude;
                $data['longitude']         = $data_npsn->longitude;
                $data['province']          = $data_npsn->propinsi;
                $data['city']              = $data_npsn->kab;
                $data['district']          = $data_npsn->kecamatan;
                $data['post_code']         = $data_npsn->kode;
                $data['level']             = $data_npsn->jenjang;
                $data['password']          = sha1(rand());
                $data['ip']                = '127.0.0.1';
                $data['cp_name']           = $cp_name;
                $data['baseurl']           = $url;
                $data['cp_email']          = $email;
                $data['input_date']        = date("Y-m-d H:i:s");

            
              //email body start
                $content  = 'Yth. '.$cp_name.','."<br/>";
                $content .= 'Berikut data konfigurasi simpul UCS untuk '.$data['name']."<br/>";
                $content .= 'Node ID : '.$data['node_id'] ."<br/>";
                $content .= 'Password : '.$data['password'] ."<br/>";
                $content .= 'Alamat peladen : http://psbsekolah.kemdikbud.go.id/kamaya/'."<br/>";
              // email body end

                $mail = new email();
                $email = $mail->sendTo($email,$cp_name);
                $email = $mail->setTemplate(ADMIN_THEMES_BASE_DIR.'mail_template.tpl.php');
                $email = $mail->addSubject('Pendaftaran Simpul UCS Kamaya');
                $email = $mail->addContent($content);
                $email = $mail->send();

                if($email == true){
                	$sql_op = new simbio_dbop($dbs);
                	$insert = $sql_op->insert('nodes', $data);  
                 	if ($insert) {        
                       		$msg .= __('configuration successfully submit to ').' '.$data['cp_email'];
			}else{
                       		$msg .= __('gagal tersimpan ');
			}
                    }else{
                        $msg .= __('message failed to send');
                    }
           
            }
            else{
                $msg .= __('school not found');
            }
        }
        
    }
    echo $msg . '</div>';
}

?>
<form method="POST" enctype="multipart/form-data">
  <div class="form-group row">
    <label for="inputNPSN" class="col-sm-4 col-form-label">NPSN</label>
    <div class="col-sm-8">
      <input type="text" name="npsn" class="form-control" id="inputNPSN" placeholder="" required>
    </div>
  </div>

  <!--<div class="form-group row">
    <label for="inputKey" class="col-sm-4 col-form-label">Kode Unik</label>
    <div class="col-sm-8">
      <input type="text" name ="unique_code" class="form-control" id="inputKey" placeholder="" required>
    </div>
  </div> --> 

  <div class="form-group row">
    <label for="inputOperator" class="col-sm-4 col-form-label">Nama Operator</label>
    <div class="col-sm-8">
      <input type="text" name="operator_name" class="form-control" id="inputOperator" placeholder="" required>
    </div>
  </div>

  <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
    <div class="col-sm-8">
      <input type="text" name="email" class="form-control" id="inputEmail" placeholder="" required>
    </div>
  </div>

  <div class="form-group row">
    <label for="inputBaseurl" class="col-sm-4 col-form-label">Url</label>
    <div class="col-sm-8">
      <input type="text" name="baseurl" class="form-control" id="inputBaseurl" placeholder="isikan jika perpustakaan sudah online">
    </div>
  </div>

  <div class="form-group row">
    <div class="col-sm-10">
        <div class="g-recaptcha" data-sitekey="<?php echo $sysconf['recaptcha']['site_key'];?>"></div>
    </div>
  <div class="col-sm-2  pull-right">
  <input type="submit" class="btn btn-warning pull-right" value="Register" name="register"/>
  </div>
  </div> 
</form>
<script src='https://www.google.com/recaptcha/api.js'></script>
