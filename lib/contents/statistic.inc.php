<?php
/**
 * Copyright (C) 2007,2008  Arie Nugraha (dicarve@yahoo.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

// be sure that this file not accessed directly
if (!defined('INDEX_AUTH')) {
    die("can not access this file directly");
} elseif (INDEX_AUTH != 1) {
    die("can not access this file directly");
}

$stats_by_title = array();
$stats_by_login = array();
$stats_by_coll = array();
$stats_coll = 0;

$query = $dbs->query("select name, count(`biblio_id`) title from biblio left join nodes on biblio.node_id = nodes.node_id group by biblio.node_id order by count('biblio_id') desc limit 5 ");
while ($_d = $query->fetch_assoc()) {
    $stats_by_title[] = array(
        'name' => $_d['name'],
        'title' => $_d['title']
    );
}
$query = $dbs->query("select name, count(`poll_id`) `updates` from nodes_poll left join nodes on nodes_poll.node_id = nodes.node_id group by nodes_poll.node_id order by count(poll_id) desc limit 5 ");
while ($_d = $query->fetch_assoc()) {
    $stats_by_login[] = array(
        'name' => $_d['name'],
        'update' => $_d['updates']
    );
}
$query = $dbs->query("select biblio_id, title from biblio order by post_date desc limit 5");
while ($_d = $query->fetch_assoc()) {
    $stats_by_coll[] = array(
        'biblio_id' => $_d['biblio_id'],
        'title' => $_d['title']
    );
}

$query = $dbs->query("select count(biblio_id) `total` from biblio");
$_d = $query->fetch_assoc();
$stats_coll = number_format($_d['total'], 0,',','.');

?>
