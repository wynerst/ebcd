<?php
class npsn
{
	public $url = 'http://referensi.data.kemdikbud.go.id';

	function __construct()
	{
		$file_headers = @get_headers($this->url);
		if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
			$this->result = false;
		}
	}

/*
	public function getnpsn($npsn)
	{
		$this->result = false;
		$data = array();
		$htmlResult = file_get_contents($this->url . '/tabs.php?npsn=' . $npsn);
		$instData = explode('id="tabs-1">', $htmlResult); //data lembaga
		$instData = explode('</table>', $instData[1]);
		$arrInst = explode('<td valign="top" width="70%">', $instData[0]);
		$arrInst = explode('<table>', $arrInst[1]);
		$arrInst = explode('<tr>', $arrInst[1]);
		foreach($arrInst as $key => $value) {
			$val = explode('<td>', $value);
			if (isset($val[2])) {
				$data[preg_split("/[\s,\/\W]+/", trim(preg_replace('/(<([^>]+)>)/i', '', strtolower($val[2])))) [0]] = trim(preg_replace('/(<([^>]+)>)/i', '', $val[4]));
			}
		}
		$maps = explode('id="tabs-8">', $htmlResult); //maps
		$maps = explode('iframe', $maps[1]);
		$maps = explode('=', $maps[1]);
		$data['latitude'] = preg_split('/[\&]+/', $maps[2]) [0];
		$data['longitude'] = preg_split('/[\&]+/', $maps[3]) [0];
		if ($data['npsn'] != '') {
			$this->result = json_decode(json_encode($data) , FALSE);
		}
	}
*/
	public function getnpsn($npsn)
	{
		$this->result = false;
		$data = array();
		$htmlResult = file_get_contents($this->url . '/tabs.php?npsn=' . $npsn);
		$instData = explode('id="tabs-1">', $htmlResult); //data lembaga
		$instData = explode('</table>', $instData[1]);
		$arrInst = explode('<td valign="top" width="70%">', $instData[0]);
		$arrInst = explode('<table>', $arrInst[1]);
		$arrInst = explode('<tr>', $arrInst[1]);
		foreach($arrInst as $key => $value) {
			$val = explode('<td>', $value);
			if (isset($val[2])) {
				$data[preg_split("/[\s,\/\W]+/", trim(preg_replace('/(<([^>]+)>)/i', '', strtolower($val[2])))) [0]] = trim(preg_replace('/(<([^>]+)>)/i', '', $val[4]));
			}
		}
		$maps = explode('id="tabs-8">', $htmlResult); //maps
		$maps = explode('iframe', $maps[1]);
		$maps = explode('=', $maps[1]);

		$a =  explode('id="tabs-8">', $htmlResult);
		$a =  explode('iframe', $a[1]);

		$re = '/(?<=marker\(\[)(.*)(?=\]\).addTo)/mi';
		preg_match_all($re, $a[0], $matches, PREG_SET_ORDER, 0);
        $location = explode(',', $matches[0][0]);
		$data['latitude'] = $location[0];
		$data['longitude'] = $location[1];

		if ($data['npsn'] != '') {
			$this->result = json_decode(json_encode($data) , FALSE);
		}
	}

	public function result()
	{
		return $this->result;
	}

}
