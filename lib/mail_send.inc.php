<?php
class email{

	public function addSubject($str)
	{
		$this->subject = $str;
	}

	public function setTemplate($str)
	{
		$this->template = $str;
	}

	public function addContent($str)
	{
		ob_start();
		$content = $str;
		include($this->template);
		$body_content = ob_get_clean();
		$this->body = $body_content; 
	}

	public function sendTo($mail,$name)
	{
		$this->mailFrom = $mail;
		$this->mailFromname = $name;
	}

	public function send()
	{	
		global $sysconf;
		require LIB_DIR . 'phpmailer/class.phpmailer.php';
        include LIB_DIR . 'phpmailer/class.smtp.php';
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host       = $sysconf['mail']['server'];
        $mail->SMTPAuth   = $sysconf['mail']['auth_enable'];
        $mail->Username   = $sysconf['mail']['auth_username'];
        $mail->Password   = $sysconf['mail']['auth_password'];
        $mail->SMTPSecure = $sysconf['mail']['SMTPsecure'];
        $mail->Port       = $sysconf['mail']['server_port'];
        $mail->setFrom($sysconf['mail']['from'], $sysconf['mail']['from_name']);
        $mail->addReplyTo($sysconf['mail']['from'], $sysconf['mail']['from_name']);
        $mail->addAddress($this->mailFrom);
        $mail->Subject = $this->subject;
        $mail->isHTML(true);
		$mail->Body = $this->body;                  
        return  $mail->send();
	}

}
