        <div class="ucs-page ucs-jumbotron">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-9">
                        <p class="text-center">
                        <?php if ($is_adv) : ?>
                            <?php printf(__('Found <strong>%s</strong> from your keywords and query took <strong>%s seconds</strong> to complete.'), $biblio_list->num_rows, $biblio_list->query_time) ?>
                            <?php 
                            $key = '<br>';
                            if ($title) { $key .= __('Title').': <strong>'.$title.'</strong> | '; }
                            if ($author) { $key .= __('Author').': <strong>'.$author.'</strong> | '; }
                            if ($subject) { $key .= __('Subject').': <strong>'.$subject.'</strong> | '; }
                            if ($isbn) { $key .= 'ISBN/ISSN : <strong>'.$isbn.'</strong> | '; }
                            if ($gmd) { $key .= 'GMD : <strong>'.$gmd.'</strong> | '; }
                            // strip last comma
                            // $key = substr_replace($key, '', -2);
                            echo $key;
                            ?>
                        <?php else : ?>
                            <?php printf(__('Found <strong>%s</strong> from your keywords and query took <strong>%s seconds</strong> to complete.'), $biblio_list->num_rows, $biblio_list->query_time) ?>
                        <?php endif ?>
                        </p>
                    </div>
                    <div class="col-12 col-lg-9">
                        <form action="index.php" class="ucs-form-search d-none d-md-block mt-4">
                            <div class="input-group">
                                <input type="hidden" name="search" value="Search">
                                <input type="text" name="keywords" class="form-control border-0" placeholder="e.g Library System">
                                <span class="input-group-addon border-0 bg-white">
                                    <select name="node" class="form-control border-0" >
                                        <?php echo $location_list ?>
                                    </select>
                                </span>
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-light">
                                        <?php echo __('Search') ?>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <form action="index.php" class="d-block d-md-none">
                            <input type="hidden" name="search" value="Search">
                            <input type="text" name="keywords" class="form-control form-control-lg border-0 mb-2" placeholder="e.g Library System">
                            <select name="node" class="form-control border-0 mb-2 form-control-lg">
                                <?php echo $location_list ?>
                            </select>
                            <button type="submit" class="btn btn-lg ucs-btn-primary btn-block">
                                <?php echo __('Search') ?>
                            </button>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-12">
                        <div class="mt-3 text-center">
                            <a class="btn btn-primary btn-sm" id="toggleAdvanceSearch" href="#" data-toggle="modal" data-target="#modalAdvanceSearch">Advanced Search</a>
                            <?php if (isset($biblio_list) && isset($sysconf['enable_xml_result']) && $sysconf['enable_xml_result']) : ?>
                            <a href="index.php?resultXML=true&<?php echo $_SERVER['QUERY_STRING'] ?>" class="xmlResultLink btn btn-danger btn-sm" target="_blank" title="View Result in XML Format" style="clear: both;">XML Result</a>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

