<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $page_title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Union Catalog Server, Library, Catalog, Union Catalog">
    <meta name="author" content="SLiMS, Senayan Developer Community">
    <?php echo $metadata; ?>
    <link rel="stylesheet" href="<?php echo $sysconf['template']['theme_folder'] ?>/less/style.css?<?php echo date('this')?>">
    <link rel="shortcut icon" href="lib/favicon.ico">
</head>
<body>
    <header class="ucs-header">

        <div class="ucs-slider">
            <div class="container">
                <nav class="navbar navbar-expand-lg">
                    <button class="navbar-toggler navbar-toggler-center" type="button" data-toggle="collapse" data-target="#navbarTogglerMain" aria-controls="navbarTogglerMain" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerMain">
                        <?php
                        $nav = array(
                            'home' => array(__('Home'), ''),
                            'libinfo' => array(__('About Us'), '')
                        );
                        $subnav = array(
                            'register' => array(__('Join Us'), ''),
                            'help' => array(__('Help'), '')
                        );
                        $schils = array(
                            'manual' => array(__('Manual'), 'https://psbsekolah.kemdikbud.go.id/schils/man/Panduan_Penggunaan_SchILS.pdf'),
                            'schils' => array(__('Download Schils'), 'https://psbsekolah.kemdikbud.go.id/schils/app/')
                        );
                        ?>
                        <ul class="navbar-nav mr-auto">
                            <?php foreach($nav as $k=>$n) : ?>
                            <li class="nav-item <?php echo ((isset($_GET['p']) && $_GET['p'] === $k) || ((empty($_SERVER['QUERY_STRING']) && $k === 'index'))) ? 'active' : '' ?>"><a class="nav-link" href="<?php echo (substr($n[1], 0, 4) == 'http') ? $n[1] : $n[1].'?p='.$k ?>"><?php echo $n[0] ?></a></li>
                            <?php endforeach ?>
                            <li>
                                <div class="dropdown show m-2">
                                    <a class="dropdown-toggle" href="#" role="button" id="dropdownSchils" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Schils
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownSchils">
                                    <?php foreach($schils as $s=>$c) : ?>
                                        <a style="color:#000" class="dropdown-item" href="<?php echo (substr($c[1], 0, 4) == 'http') ? $c[1] : $c[1].'?p='.$s ?>"><?php echo $c[0] ?></a>
                                    <?php endforeach ?>
                                    </div>
                                </div>                           
                            </li>
                        </ul>                        
                        <ul class="navbar-nav">
                            <?php foreach($subnav as $k=>$s) : ?>
                            <li class="nav-item <?php echo (isset($_GET['p']) && $_GET['p'] === $k) ? 'active' : '' ?>"><a class="nav-link" href="<?php echo $s[1].'?p='.$k ?>"><?php echo $s[0] ?></a></li>
                            <?php endforeach ?>
                            <li>
                                <div class="dropdown show m-2">
                                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php echo __('Change Language') ?>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <?php echo $language_list ?>
                                    </div>
                                </div>                           
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>
        </div>

        <div class="ucs-brand">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-7">
                        <h1 class="navbar-title"><a href="index.php"><?php echo $sysconf['server']['name'] ?></a></h1>
                        <div class="navbar-tagline"><?php echo $sysconf['library_subname'] ?></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-4 col-md-2">
                        <img width="42" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIyNTZweCIgaWQ9IkxheWVyXzEiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDI1NiAyNTY7IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAyNTYgMjU2IiB3aWR0aD0iMjU2cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+LnN0MHtmaWxsOm5vbmU7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEwO308L3N0eWxlPjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0yMjguNjQ5LDE3NC42MDZjMjcuMTEzLTU0LjU3LDMuNDg5LTEyMC41OTUtNDguNjA3LTE0NC4wMTNDMTI5LjE3OCw3LjcyOCw2OC40OTcsMzIuNTIyLDQ0LjM4OCw4MS44ODQgIGMtMjQuNDQxLDUwLjA0Mi01LjUxMSwxMTMuNzI5LDQ0LjIxNCwxMzkuMzg2Yzc3Ljk1NiwzOS42NDMsMTMwLjcyNy0yNy4zOTMsMTMxLjUyNS0zMS4xNTIiLz48cGF0aCBjbGFzcz0ic3QwIiBkPSJNMTQyLjI4NSwxMzQuMzk2YzQuNTQtMy44OTIsMTEuNzQ1LTguMjg2LDExLjMxMy0xMC42NjVjLTAuNDMyLTIuMzc4LTQuOTA0LTcuMTk4LTEzLjA0Mi03LjcxMiAgYy0wLjk2MS0wLjA2MS0xNS41MjMtMC42OTEtMjIuMTk4LDkuOTQ1Yy01LjQzOCw4LjY2NS0zLjAzMSwxOC41NzMsMi4zNzgsMjUuMzY2YzYuNTg5LDguMjc0LDE5LjI2OSwxMC40NiwyOS40MDMsNC44OTggIGMxOC43MDEtMTMuMDgsOC4yNTMtMjYuNTg5LDkuNDQyLTI5LjYxNWM5LjI5Ni0xNC43MDIsMzcuMzI4LTExLjI0MiwzOS40OS0xMi43NTZjMi4xNjItMS41MTMsMy44OTItMTYuMTA3LDIuMzc4LTE4LjgwOSAgcy0yOS41OTEtMjQuMDcxLTY2LjA4Mi0yMi43MDFDMTA0LjY1MSw3My41LDgxLjIxLDkwLjgzMiw3OC45MzksOTMuOTY3Yy0yLjI3LDMuMTM1LTMuODkyLDIwLjc1NS0xLjczLDIyLjA1MiAgczI5LjI5NSwxLjQwNSwzMS4zNDktMC4yMTZjMi4wNTQtMS42MjEtMi43MDItMTYuOTcyLTAuODY1LTIwLjEwN2MxLjgzOC0zLjEzNSwxNS44OTgtNi43MzYsMjkuMTg3LTcuMTM1ICBjMTUuMzc3LTAuNDYyLDMxLjQ1Nyw0LjMyNCwzMy4wNzksNi45MThjMS42MjIsMi41OTQtMS43MywxNC4wNTMtMS4wODEsMTYuNDMxYzAuNjQ5LDIuMzc4LDIwLjgyNiwzMi4zMjIsMjAuMTc4LDQ2LjQ4MyAgYy0wLjY0OSwxNC4xNjEtNS43MywxOS41NjYtNy41NjcsMjAuMzIzYy0xLjgzNywwLjc1Ny04NS43NTYsMS4xODktODcuOTE4LTAuNDMyYy0yLjE2Mi0xLjYyMi01LjE4OS0xMy41MTMtNC43NTctMjMuMTMzICBzNy4yNTMtMjQuNTczLDEwLjk1Mi0zMS41NjUiLz48L3N2Zz4=" alt="Hotline">
                        <p><?php echo __('Hotline') ?></p>
                        <strong><a href="tel:+62215707870">+6221-5707870</a></strong>
                    </div>

                    <div class="col-4 col-md-3">
                        <img width="42" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjxzdmcgaGVpZ2h0PSIyNTZweCIgaWQ9IkxheWVyXzEiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDI1NiAyNTY7IiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAyNTYgMjU2IiB3aWR0aD0iMjU2cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+LnN0MHtmaWxsOm5vbmU7c3Ryb2tlOiMwMDAwMDA7c3Ryb2tlLXdpZHRoOjQ7c3Ryb2tlLWxpbmVjYXA6cm91bmQ7c3Ryb2tlLWxpbmVqb2luOnJvdW5kO3N0cm9rZS1taXRlcmxpbWl0OjEwO308L3N0eWxlPjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik05MC4xMjIsMTExLjMzNmM0LjgxNCwxMC42OTgsNDAuNDc1LDQxLjkwMSw0My44NjMsNDIuMDhzNTguNjYyLTQ5LjM5LDU3LjA1Ny01Mi43NzggIFM3OS4yNDYsOTYuMTgsNzYuMjE0LDk5LjU2OGMtMy4wMzEsMy4zODgtMS45NjIsNzguMDk3LDAuNzEzLDgwLjIzN3MxMTEuMDgzLDIuMzE4LDExMy40MDEtMS40MjYgIGMyLjMxOC0zLjc0NCw0LjgxNC01Ny40MTQsMS43ODMtNjAuNjIzIi8+PHBhdGggY2xhc3M9InN0MCIgZD0iTTEwOC4yNjgsMjM3LjM1MmM1OS4wMzYsMTUuMDg5LDExOC42NTMtMjEuODMyLDEzMC42NDgtNzcuNjc2YzExLjcxMS01NC41MjQtMjUuMjM2LTEwOC42NzEtNzguNTUxLTEyMS45MTIgIEMxMDYuMzE1LDI0LjM0LDQ4LjAwMiw1Ni4xODMsMzMuMzIxLDExMC4xNzZjLTIyLjQ0Nyw4NC41MjcsNTQuMTUxLDEyMi4wOTcsNTcuOTk0LDEyMi4wOSIvPjwvc3ZnPg==" alt="Email">
                        <p><?php echo __('Email') ?></p>
                        <strong><a href="mailto:kamaya@kemdikbud.go.id">kamaya@kemdikbud.go.id</a></strong>
                    </div>

                </div>
            </div>
        </div>

        <div class="ucs-nav" id="nav">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon navbar-toggler-icon-alt"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <?php $navmerge = array_merge($nav, $subnav); ?>
                            <?php foreach($navmerge as $n) : ?>
                            <li class="nav-item <?php echo ((isset($_GET['p']) && $_GET['p'] === $k) || ((empty($_SERVER['QUERY_STRING']) && $k === 'index'))) ? 'active' : '' ?>"><a class="nav-link text-nowrap" href="<?php echo $n[1] ?>"><?php echo $n[0] ?></a></li>
                            <?php endforeach ?>
                            <li>
                                <div class="dropdown show mt-2">
                                    <a class="dropdown-toggle" href="#" role="button" id="dropdownSchils" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Schils
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownSchils">
                                    <?php foreach($schils as $s=>$c) : ?>
                                        <a style="color:#000" class="dropdown-item" href="<?php echo (substr($c[1], 0, 4) == 'http') ? $c[1] : $c[1].'?p='.$s ?>"><?php echo $c[0] ?></a>
                                    <?php endforeach ?>
                                    </div>
                                </div>                           
                            </li>
                            <li>
                                <div class="dropdown show mt-3 mb-3">
                                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?php echo __('Change Language') ?>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <?php echo $language_list ?>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <form action="index.php" class="form-inline my-2 my-lg-0">
                            <input name="keywords" class="form-control mr-sm-2" type="search" placeholder="<?php echo __('e.g Library System') ?>" aria-label="Search">
                            <select name="node" id="locations" class="form-control mt-2 mr-sm-2">
                                <?php echo $location_list ?>
                            </select>
                            <input type="hidden" name="search" value="Search">
                            <button class="btn ucs-btn-primary my-2 my-sm-0" type="submit"><?php echo __('Search') ?></button>
                        </form>
                    </div>
                </nav>
            </div>
        </div>

    </header>

    <main>
    <?php if(isset($_GET['search'])) {
        require 'page.php'; ?>
        <div class="ucs-page ucs-page-alt ucs-page-detail flex-row d-md-flex ">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="card ucs-card d-none d-md-block">
                            <div class="card-header">
                                <strong class="text-large"><?php echo __('Information') ?></strong>
                            </div>
                            <div class="card-body mt-3">
                                <?php echo $info ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-8">
                        <?php echo $main_content ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } elseif(isset($_GET['p'])) { ?>
        <?php if($_GET['p'] == 'show_detail' || $_GET['p'] == 'maps' || $_GET['p'] == 'map') : ?>
          <div class="container mt-4">
            <?php echo $main_content ?>
          </div>
        <?php else : ?>
        <?php require 'content.php'; ?>
        <div class="ucs-page ucs-page-alt ucs-page-detail flex-row d-md-flex">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <?php echo $main_content ?>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="card ucs-card d-none d-md-block">
                            <div class="card-header">
                                <strong class="text-large"><?php echo __('Information') ?></strong>
                            </div>
                            <div class="card-body mt-3">
                                <?php echo $info ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php endif ?>
    <?php } else {
        require 'home.php';
    } ?>
    </main>

    <footer class="ucs-footer">
        <div class="container">
            <div class="row ucs-footer-body">
                <div class="col-12 col-md-4">
                    <img src="<?php echo $sysconf['template']['theme_folder'] ?>/img/logo.png" width="64" alt="">
                    <p class="pt-4">
                        <strong><?php echo $sysconf['library_subname'] ?></strong>
                    </p>
                    <address class="text-sm">
                        Gedung A Lantai 1 dan Mezanin<br>
                        Jl. Jenderal Sudirman, Senayan – Jakarta<br>
                        Indonesia, 10270<br><br>
                        Telp. +6221 5707870<br>
                        Fax. +6221 5731228<br>
                        Email: kamaya@kemdikbud.go.id
                    </address>
                </div>
                <div class="col-12 col-md-5">
                    <br>
                    <strong><?php echo __('Other Links') ?></strong>
                    <br><br>
                    <ul>
                      <li>
                        <a href="https://www.kemdikbud.go.id/" target="_blank">Kementerian Pendidikan dan Kebudayaan</a>
                      </li>
                      <li>
                        <a href="http://perpustakaan.kemdikbud.go.id/laman" target="_blank">Perpustakaan Kementerian Pendidikan dan Kebudayaan</a>
                      </li>
                      <li>
                        <a href="https://psbsekolah.kemdikbud.go.id" target="_blank">PSB Sekolah</a>
                      </li>
                      <li>
                        <a href="http://slims.web.id/" target="_blank">SLiMS Library Management System</a>
                      </li>
	            </ul>
                </div>
                <div class="col-12 col-md-3">
                    <br>
                    <strong><?php echo __('Find us here') ?></strong>
                    <br><br>
                    <a href=""><img src="<?php echo $sysconf['template']['theme_folder'] ?>/img/facebook.png" alt=""></a>
                    <a href=""><img src="<?php echo $sysconf['template']['theme_folder'] ?>/img/twitter.png" alt=""></a>
                    <a href=""><img src="<?php echo $sysconf['template']['theme_folder'] ?>/img/instagram.png" alt=""></a>
                    <a href=""><img src="<?php echo $sysconf['template']['theme_folder'] ?>/img/youtube.png" alt=""></a>
                    <br><br>
                    <a href="http://onesearch.id/Search/Results?widget=1&repository_id=5236" target="_blank"><img src="<?php echo $sysconf['template']['theme_folder'] ?>/img/logo-ios.png" width="200" alt=""></a>
                    <br><br>
                    <p></p>
                </div>
            </div>
            <div class="ucs-copyright">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <p><?php echo $sysconf['server']['name'] ?> &copy; 2017 Perpustakaan Kemdikbud</p>
                    </div>
                    <div class="col-12 col-md-6">
                        <p class="text-right d-none d-md-block"><?php echo __('Development by') ?> Pustekkom & Senayan Developers Community</p>
                        <p class="d-block d-md-none"><?php echo __('Development by')?> Pustekkom & Senayan Developers Community</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <div class="modal fade bd-example-modal-lg ucs-modal" id="modalAdvanceSearch" tabindex="-1" role="dialog" aria-labelledby="modalAdvanceSearchLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content border-0">
                <div class="ucs-modal-sidebar"></div>
                <form action="index.php">
                <div class="modal-header mt-4 border-0">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col">
                                <h4 class="lead font-weight-bold"><?php echo __('Advanced Search') ?></h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="title"><?php echo __('Title') ?></label>
                                    <input type="text" name="title" id="title" class="form-control"  aria-describedby="<?php echo __('Title') ?>">
                                </div>
                                <div class="form-group">
                                    <label for="subject"><?php echo __('Subject(s)') ?></label>
                                    <input type="text" name="subject" id="subject" class="form-control"  aria-describedby="<?php echo __('Subject(s)') ?>">
                                </div>
                                <div class="form-group">
                                    <label for="gmd">GMD</label>
                                    <select name="gmd" id="gmd" class="form-control">
                                        <?php echo $gmd_list ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="form-group">
                                    <label for="author"><?php echo __('Author(s)') ?></label>
                                    <input type="text" name="author" id="author" class="form-control"  aria-describedby="<?php echo __('Author(s)') ?>">
                                </div>
                                <div class="form-group">
                                    <label for="isbn"><?php echo __('ISBN/ISSN') ?></label>
                                    <input type="text" name="isbn" class="form-control" id="subject" aria-describedby="ISBN/ISSN">
                                </div>
                                <div class="form-group">
                                    <label for="location"><?php echo __('Location') ?></label>
                                    <select name="location" id="gmd" class="form-control">
                                        <?php echo $location_list ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer border-0 mb-4">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col text-right">
                                <input type="hidden" name="search" value="search">
                                <button type="cancel" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Cancel') ?></button>
                                <button type="submit" class="btn btn-primary"><?php echo __('Search') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo $sysconf['template']['theme_folder'] ?>/js/jquery-slim.min.js"></script>
    <script src="<?php echo $sysconf['template']['theme_folder'] ?>/js/popper.min.js"></script>
    <script src="<?php echo $sysconf['template']['theme_folder'] ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $sysconf['template']['theme_folder'] ?>/js/custome.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-63387887-3"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-63387887-3');
    </script>
</body>
</html>
