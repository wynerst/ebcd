<div class="ucs-page ucs-jumbotron">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <h3 class="text-center font-weight-bold"><?php echo __('Collections') ?></h3>
                <p class="lead text-center"><?php printf(__('There are currently <strong>%s</strong> of collections that collected through using <strong>Schils</strong>.'), $stats_coll) ?></p>
            </div>
            <div class="clearfix"></div>
            <div class="col-12 col-lg-9">
                <form action="index.php" class="ucs-form-search d-none d-md-block mt-4">
                    <div class="input-group">
                        <input type="hidden" name="search" value="Search">
                        <input type="text" name="keywords" class="form-control border-0" placeholder="<?php echo __('e.g Library System') ?>">
                        <span class="input-group-addon border-0 bg-white">
                            <select name="node" class="form-control border-0" >
                                <?php echo $location_list ?>
                            </select>
                        </span>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary">
                                <?php echo __('Search') ?>
                            </button>
                        </span>
                    </div>
                </form>

                <form action="index.php" class="d-block d-md-none">
                    <input type="hidden" name="search" value="Search">
                    <input type="text" name="keywords" class="form-control form-control-lg border-0 mb-2" placeholder="<?php __('e.g Library System') ?>">
                    <select name="node" class="form-control border-0 mb-2 form-control-lg">
                        <?php echo $location_list ?>
                    </select>
                    <button type="submit" class="btn btn-primary btn-block">
                        <?php echo __('Search') ?>
                    </button>
                    </div>
                </form>

            </div>
            <div class="clearfix"></div>
            <div class="col-12">
                <div class="mt-3 text-center">
                    <a class="btn btn-primary btn-sm" id="toggleAdvanceSearch" href="#" data-toggle="modal" data-target="#modalAdvanceSearch"><?php echo __('Advanced Search'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

            <?php 
            $node = json_decode($node_location);
            $a = array();
            foreach ($node as $key => $value) {
              if($value[1]!='' || $value[2]!=''){
              $a[] = array('lat'=>$value[1],'lng'=>$value[2],'title'=>'\''.str_replace('</b>','',str_replace('<b>','',str_replace('<br/>',"\n",$value[3]))).'\'');
            }
            }

             $node_s = str_replace('"','',json_encode($a));
             ?>


<div class="ucs-page ucs-page-alt flex-row d-md-flex">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 mb-4">
                <div class="card ucs-card">
                    <div class="card-body text-center mt-3">
                        <strong class="text-large"><?php echo __('Most Large Collection') ?></strong>
                    </div>
                    <div class="text-center mb-5">
                        <img src="<?php echo $sysconf['template']['theme_folder'] ?>/img/book.png" alt="">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center bg-light"><b><?php echo __('Source') ?></b><span><b><?php echo __('Collections') ?></b></span></li>
                        <?php foreach($stats_by_title as $title) : ?>
                        <li class="list-group-item d-flex justify-content-between align-items-center"  title="<?php echo $title['name'] ?>"><?php echo ucwords(substr($title['name'],0,25)); echo (strlen($title['name'])>25 ) ? '...' : '' ?> <span class="text-primary font-weight-bold"><?php echo number_format($title['title'],0,',','.') ?></span></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>

            <div class="col-12 col-md-4 mb-4">
                <div class="card ucs-card">
                    <div class="card-body text-center mt-3">
                        <strong class="text-large"><?php echo __('Most Active Library') ?></strong>
                    </div>
                    <div class="text-center mb-5">
                        <img src="<?php echo $sysconf['template']['theme_folder'] ?>/img/pencils.png" alt="">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item d-flex justify-content-between align-items-center bg-light"><b><?php echo __('Source') ?></b><span><b><?php echo __('Uploaded') ?></b></span></li>
                        <?php foreach($stats_by_login as $login) : ?>
                        <li class="list-group-item d-flex justify-content-between align-items-center" title="<?php echo $login['name'] ?>"><?php echo ucwords(substr($login['name'],0,25)); echo (strlen($login['name'])>25 ) ? '...' : '' ?> <span class="text-primary font-weight-bold"><?php echo number_format($login['update'],0,',','.') ?></span></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>

            <div class="col-12 col-md-4 mb-4">
                <div class="card ucs-card">
                    <div class="card-body text-center mt-3">
                        <strong class="text-large"><?php echo __('Latest Collections') ?></strong>
                    </div>
                    <div class="text-center mb-5">
                        <img src="<?php echo $sysconf['template']['theme_folder'] ?>/img/bag.png" alt="">
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item  bg-light"><b><?php echo __('Title') ?></b></li>
                        <?php foreach($stats_by_coll as $coll) : ?>
                        <li class="list-group-item text-truncate"><?php echo $coll['title'] ?></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="ucs-page-alt flex-row d-md-flex" style="margin-bottom: 70px;">
    <div class="container">
        <div class="row">
                    <div class="col-12 col-md-12 mb-12">

                    <div class="card-body text-center mt-3">
                        <strong class="text-large">Jumlah sekolah tergabung : <?php echo count(json_decode($node_location))?></strong>
                    </div>
    <div id="map" style="height:500px;"></div>
                    </div>

        </div>
    </div>
</div>

<script>
      var test = <?php echo $node_s; ?>;

      function initMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 5,
          center: {lat: -2.0097465, lng: 117.829518}

        });
        var markers = locations.map(function(location, i) {
          return new google.maps.Marker({
            position: location,
            //icon: './themes/default/images/logo.png', 
            title: location['title'],
             zIndex: 1   
          });
        });

        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, markers,
            {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
      }
      var locations = test
    </script>
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBZY7bxlwBh14vQZh3FogMPZcInxT6tgc&callback=initMap"></script>