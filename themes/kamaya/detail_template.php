<?php ob_start() ?>

<div class="ucs-page ucs-jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <strong>{gmd_name}</strong>
                <h3 class="mt-2 font-weight-bold">{title}</h3>                    
            </div>
        </div>
    </div>
</div>

<div class="ucs-page ucs-page-alt">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4">
                <div class="card ucs-card d-none d-md-block">
                    <div class="card-header">
                        <strong><?php echo __('Detail') ?></strong>                        
                    </div> 
                    <div class="card-body text-center mt-3">
                        <div class="text-center mb-5">
                            {ucs_image}
                        </div>
                        <strong>{location}</strong>
                        <div>
                            <a href="javascript: history.back();" class="btn btn-sm btn-primary"><?php print __('Back To Previous'); ?></a>
                            <?php if (isset($sysconf['enable_xml_result']) && $sysconf['enable_xml_result']) : ?>
                            <a href="index.php?p=show_detail&inXML=true&id=<?php echo $_GET['id'] ?>" class="xmlResultLink btn btn-danger btn-sm" target="_blank" title="View Result in XML Format" style="clear: both;">XML Result</a>
                            <?php endif ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <div class="card ucs-card">
                    <p class="m-3">{notes}</p>
                    <table class="table">
                        <tr>
                            <td><b><?php echo __('Author(s)') ?></b></td>
                            <td><span>{authors}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Edition') ?></b></td>
                            <td><span>{edition}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Call Number') ?></b></td>
                            <td><span>{call_number}</span></td>
                        </tr>                        
                        <tr>
                            <td><b><?php echo __('ISBN/ISSN') ?></b></td>
                            <td><span>{isbn_issn}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Subject(s)') ?></b></td>
                            <td><span>{subjects}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Classification') ?></b></td>
                            <td><span>{classification}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Language') ?></b></td>
                            <td><span>{language_name}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Publisher') ?></b></td>
                            <td><span>{publisher_name}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Publishing Year') ?></b></td>
                            <td><span>{publish_year}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Publishing Place') ?></b></td>
                            <td><span>{publish_place}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Collation') ?></b></td>
                            <td><span>{collation}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Specific Detail Info') ?></b></td>
                            <td><span>{spec_detail_info}</span></td>
                        </tr>
                        <tr>
                            <td><b><?php echo __('Read Online') ?></b></td>
                            <td><span>{ucs_attachment}</span></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php 
$detail_template = ob_get_clean(); 
?>

