<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $page_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Union Catalog Server, Library, Catalog, Union Catalog">
    <meta name="author" content="SLiMS, Senayan Developer Community">
		<?php echo $metadata; ?>
    <link rel="stylesheet" href="<?php echo $sysconf['template']['theme_folder'] ?>/css/bootstrap.min.css?<?php echo date('this')?>">
    <link rel="stylesheet" href="<?php echo $sysconf['template']['theme_folder'] ?>/css/signin.css?<?php echo date('this')?>">
    <link rel="shortcut icon" href="lib/favicon.ico">

    <script src="./js/jquery.js"></script>

    <!-- For IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="./themes/default/lib/html5.js"></script>
    <![endif]-->

  </head>

  <body class="login">

  <noscript><div style="font-weight: bold; color: #F00;"><?php echo __('Your browser does not support Javascript or Javascript is disabled. Application won\'t run without Javascript!'); ?><div></noscript>

    <div class="d-flex justify-content-center">
      <div id="main-content" style="width: 20em">
        <?php echo $main_content; ?>
      </div>		
    </div>

  </body>
</html>
