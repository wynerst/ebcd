<?php
/**
 * Copyright (C) 2010  Arie Nugraha (dicarve@yahoo.com), Wardiyono (wynerst@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/* UCS Node Management */

// key to authenticate
define('INDEX_AUTH', '1');

// main system configuration
require '../../../ucsysconfig.inc.php';
// start the session
require UCS_BASE_DIR.'admin/default/session.inc.php';
require UCS_BASE_DIR.'admin/default/session_check.inc.php';

// only administrator have privileges to modify user groups
if ($_SESSION['uid'] != 1) {
    die('<div class="errorBox">'.__('You don\'t have enough privileges to view this section').'</div>');
}

require SIMBIO_BASE_DIR.'simbio_GUI/table/simbio_table.inc.php';
require SIMBIO_BASE_DIR.'simbio_GUI/form_maker/simbio_form_table_AJAX.inc.php';
require SIMBIO_BASE_DIR.'simbio_GUI/paging/simbio_paging.inc.php';
require SIMBIO_BASE_DIR.'simbio_DB/datagrid/simbio_dbgrid.inc.php';
require SIMBIO_BASE_DIR.'simbio_DB/simbio_dbop.inc.php';

/* RECORD OPERATION */
if (isset($_POST['saveData'])) {
    $node_id = trim(strip_tags($_POST['node_id']));
    $node_name = trim(strip_tags($_POST['name']));
    $password = trim(strip_tags($_POST['password']));
    // check form validity
    if (empty($node_id) or empty($node_name)) {
        utility::jsAlert(__('Node ID and Name can\'t be empty'));
        exit();
    } elseif(!is_numeric($node_id)){
         utility::jsAlert(__('Node ID only numeric value'));
        exit();
    } elseif(empty($password)){
         utility::jsAlert(__('Password cannot be empty'));
        exit();
    } else {
        $data['node_id'] = $node_id;
        $data['name'] = $node_name;
        $data['password'] = trim($dbs->escape_string(strip_tags($_POST['password'])));
        $data['baseurl'] = trim($dbs->escape_string(strip_tags($_POST['baseurl'])));
        $data['ip'] = trim($dbs->escape_string(strip_tags($_POST['ip'])));
        $data['cp_name'] = trim($dbs->escape_string(strip_tags($_POST['cp_name'])));
        $data['cp_phone'] = trim($dbs->escape_string(strip_tags($_POST['cp_phone'])));                
        $data['cp_email'] = trim($dbs->escape_string(strip_tags($_POST['cp_email'])));
        $data['latitude'] = trim($dbs->escape_string(strip_tags($_POST['latitude'])));
        $data['longitude'] = trim($dbs->escape_string(strip_tags($_POST['longitude'])));
        $data['address'] = trim($dbs->escape_string(strip_tags($_POST['address'])));
        $data['province'] = trim($dbs->escape_string(strip_tags($_POST['province'])));
        $data['city'] = trim($dbs->escape_string(strip_tags($_POST['city'])));
        $data['district'] = trim($dbs->escape_string(strip_tags($_POST['district'])));                        
        $data['post_code'] = trim($dbs->escape_string(strip_tags($_POST['post_code'])));
       // $data['input_date'] = date('Y-m-d H:i:s');
        $data['last_update'] = date('Y-m-d H:i:s');
        if(isset($_POST['auto'])){
            require LIB_DIR .'npsn_request.inc.php';  
            $_npsn = new npsn();
            $_npsn->getnpsn($node_id);
            $data_npsn= $_npsn->result();    
            if($data_npsn!=false){
                $data['name']      = $data_npsn->nama;
                $data['address']   = $data_npsn->alamat;
                $data['status']    = $data_npsn->status;
                $data['latitude']  = $data_npsn->latitude;
                $data['longitude'] = $data_npsn->longitude;
                $data['province']  = $data_npsn->propinsi;
                $data['city']      = $data_npsn->kab;
                $data['district']  = $data_npsn->kecamatan;
                $data['post_code'] = $data_npsn->kode;
                $data['level']     = $data_npsn->jenjang;
            }            
        }

        // create sql op object
        $sql_op = new simbio_dbop($dbs);
        if (isset($_POST['updateRecordID'])) {
            /* UPDATE RECORD MODE */
            // remove input date
            unset($data['input_date']);
            // filter update record ID
            $updateRecordID = $_POST['updateRecordID'];
            // update the data
            $update = $sql_op->update('nodes', $data, 'node_id=\''.$updateRecordID.'\'');

            // update biblio node
            $biblioData['node_id'] = $node_id;
            $update_biblio = $sql_op->update('biblio', $biblioData, 'node_id=\''.$updateRecordID.'\'');

            if ($update) {
                utility::jsAlert(__('UCS Node Data Successfully Updated'));
                echo '<script type="text/javascript">parent.$(\'#mainContent\').simbioAJAX(\''.$_SERVER['PHP_SELF'].'\', {addData: \''.$_POST['lastQueryStr'].'\'});</script>';
            } else { utility::jsAlert(__('UCS Node Data FAILED to Updated. Please Contact System Administrator')."\nDEBUG : ".$error); }
            exit();
        } else {
            /* INSERT RECORD MODE */
            // insert the data
            $insert = $sql_op->insert('nodes', $data);
            if ($insert) {
                utility::jsAlert(__('New UCS Node Data Successfully Saved'));
                echo '<script type="text/javascript">parent.$(\'#mainContent\').simbioAJAX(\''.$_SERVER['PHP_SELF'].'\');</script>';
            } else {
                utility::jsAlert(__('UCS Node Data FAILED to Save. Please Contact System Administrator')."\nDEBUG : ".$sql_op->error);
            }
            exit();
        }
    }
    exit();
} else if (isset($_POST['itemID']) AND !empty($_POST['itemID']) AND isset($_POST['itemAction'])) {
    /* DATA DELETION PROCESS */
    $sql_op = new simbio_dbop($dbs);
    $failed_array = array();
    $error_num = 0;
    $node_id = $_POST['itemID'];
    $still_have_collection = array();
    if (!is_array($node_id)) {
        // make an array
        $node_id = array($node_id);
    }
    // loop array
    foreach ($node_id as $itemID) {
        // check if this node data still have an collection
        $node_coll_q = $dbs->query('SELECT n.name, COUNT(n.node_id) FROM biblio AS b
        LEFT JOIN nodes AS n ON n.node_id=b.node_id  WHERE b.node_id=\''.$itemID.'\' GROUP BY title');
        $node_coll_d = $node_coll_q->fetch_row();
	    $count_coll = $node_coll_q->num_rows;
        if ($node_coll_d[1] < 1) {    
            //delete nodes_poll data
            $sql_op->delete('nodes_poll', 'node_id=\''.$itemID.'\'');        
            if(!$sql_op->delete('nodes', 'node_id=\''.$itemID.'\'')){
                $error_num++;
            }
        }else{
            $still_have_collection[] = 'node '.substr($node_coll_d[0], 0, 45).' still have '.$count_coll.' collections';
            $error_num++;
        }
    }

  if ($still_have_collection) {
    $titles = '';
    foreach ($still_have_collection as $title) {
      $titles .= $title."\n";
    }
    utility::jsAlert(__('Below data can not be deleted: ')."\n".$titles);
    echo '<script type="text/javascript">parent.$(\'#mainContent\').simbioAJAX(\''.$_SERVER['PHP_SELF'].'\', {addData: \''.$_POST['lastQueryStr'].'\'});</script>';
    exit();
}
    // error alerting
    if ($error_num == 0) {
        utility::jsAlert(__('All Data Successfully Deleted'));
        echo '<script type="text/javascript">parent.$(\'#mainContent\').simbioAJAX(\''.$_SERVER['PHP_SELF'].'\', {addData: \''.$_POST['lastQueryStr'].'\'});</script>';
    } else {
        utility::jsAlert(__('Some or All Data NOT deleted successfully!\nPlease contact system administrator'));
        echo '<script type="text/javascript">parent.$(\'#mainContent\').simbioAJAX(\''.$_SERVER['PHP_SELF'].'\', {addData: \''.$_POST['lastQueryStr'].'\'});</script>';
    }
    exit();
}
/* RECORD OPERATION END */

/* search form */
?>
<fieldset class="menuBox">
<div class="menuBoxInner userGroupIcon">
  <div class="per_title">
      <h2><?php echo __('Member Nodes'); ?></h2>
  </div>
  <div class="sub_section">
      <div class="action_button">
      <a href="<?php echo MODULES_WEB_ROOT_DIR; ?>system/nodes.php?action=detail" class="headerText2"><?php echo __('Add New Node'); ?></a>
      &nbsp; <a href="<?php echo MODULES_WEB_ROOT_DIR; ?>system/nodes.php" class="headerText2"><?php echo __('Nodes List'); ?></a>
      </div>
    <form name="search" action="<?php echo MODULES_WEB_ROOT_DIR; ?>system/nodes.php" id="search" method="get" style="display: inline;"><?php echo __('Search'); ?> :
    <input type="text" name="keywords" size="30" />
    <input type="submit" id="doSearch" value="<?php echo __('Search'); ?>" class="button" />
    </form>
  </div>
</div>
</fieldset>
<?php
/* search form end */
/* main content */
if (isset($_POST['detail']) OR (isset($_GET['action']) AND $_GET['action'] == 'detail')) {
    /* RECORD FORM */
    $itemID = (integer)isset($_POST['itemID'])?$_POST['itemID']:0;
    $rec_q = $dbs->query('SELECT * FROM nodes WHERE node_id=\''.$itemID.'\'');
    $rec_d = $rec_q->fetch_assoc();

    // create new instance
    $form = new simbio_form_table_AJAX('mainForm', $_SERVER['PHP_SELF'].'?itemID='.$itemID, 'post');
    $form->submit_button_attr = 'name="saveData" value="'.__('Save').'" class="button"';

    // form table attributes
    $form->table_attr = 'align="center" id="dataList" cellpadding="5" cellspacing="0"';
    $form->table_header_attr = 'class="alterCell" style="font-weight: bold;"';
    $form->table_content_attr = 'class="alterCell2"';

    // edit mode flag set
    if ($rec_q->num_rows > 0) {
        $form->edit_mode = true;
        // record ID for delete process
        $form->record_id = $itemID;
        // form record title
        $form->record_title = $rec_d['name'];
        // submit button attribute
        $form->submit_button_attr = 'name="saveData" value="'.__('Update').'" class="button"';
    }

    /* Form Element(s) */
    // node id  
    $chbox_array[] = array('1', __('Enable'));
    $form->addCheckBox('auto', __('Auto update pofile'),$chbox_array);
    $form->addTextField('text', 'node_id', __('Nomor Pokok Sekolah Nasional').'*', $rec_d['node_id'], 'required style="width: 70%;"');
    $form->addTextField('text', 'name', __('School Name').'*', $rec_d['name'], 'required style="width: 70%;"');
    $form->addTextField('text', 'level', __('Level'), $rec_d['level'], 'style="width: 70%;"');
    $form->addTextField('text', 'status', __('School Status'), $rec_d['status'], 'style="width: 70%;"');
    $form->addTextField('textarea', 'address', __('School Address'), $rec_d['address'], 'style="width: 70%;" rows="2"');
    $form->addTextField('text', 'province', __('Province'), $rec_d['province'], 'style="width: 70%;"');
    $form->addTextField('text', 'city', __('City'), $rec_d['city'], 'style="width: 70%;"');
    $form->addTextField('text', 'district', __('District'), $rec_d['district'], 'style="width: 70%;"');
    $form->addTextField('text', 'post_code', __('Post Code'), $rec_d['post_code'], 'style="width: 70%;"');
    $form->addTextField('text', 'latitude', __('Latitude Coordinate'), $rec_d['latitude'], 'style="width: 70%;"');
    $form->addTextField('text', 'longitude', __('Longitude Coordinate'), $rec_d['longitude'], 'style="width: 70%;"');
    $form->addTextField('text', 'password', __('Password').'*', $rec_d['password'], 'required style="width: 70%;"');    
    $form->addTextField('text', 'baseurl', __('School URL'), $rec_d['baseurl'], 'style="width: 70%;"');
    $form->addTextField('text', 'ip', __('Node IP address'), $rec_d['ip'], 'style="width: 70%;"');
    $form->addTextField('text', 'cp_name', __('Contact Name'), $rec_d['cp_name'], 'style="width: 70%;"');
    $form->addTextField('text', 'cp_phone', __('Contact Phone'), $rec_d['cp_phone'], 'style="width: 70%;"');
    $form->addTextField('text', 'cp_email', __('Contact email'), $rec_d['cp_email'], 'style="width: 70%;"');
    // edit mode messagge
    if ($form->edit_mode) {
        echo '<div class="infoBox">'.__('You are going to edit node data').' : <b>'.$rec_d['name'].'</b> <br />'.__('Last Update ').$rec_d['last_update'].'</div>'; //mfc
    }
    // print out the form object
    echo $form->printOut();
} else {
    /* Member Node LIST */
    // table spec
    $table_spec = 'nodes AS n';

    // create datagrid
    $datagrid = new simbio_datagrid();
    $datagrid->setSQLColumn('n.node_id',
            'n.node_id AS \''.__('NPSN').'\'',
            'n.name AS \''.__('School Name').'\'',
            'n.city AS \''.__('City').'\'',
            'n.province AS \''.__('Province').'\'',
            'CONCAT(n.cp_name," (", n.cp_email ,")", IF(n.cp_email<>\'\',\'<br />\',\'\'), n.baseurl) AS \''.__('Contact/URL').'\'',
            'n.input_date AS \''.__('Input Date').'\'',
                    'n.last_update AS \''.__('Last Date').'\'');

    $datagrid->setSQLorder('n.input_date DESC');

    // is there any search
    if (isset($_GET['keywords']) AND $_GET['keywords']) {
       $keywords = $dbs->escape_string($_GET['keywords']);
       $datagrid->setSQLCriteria("n.name LIKE '%$keywords%' OR n.node_id LIKE '%$keywords%'
            OR n.cp_email LIKE '%$keywords%'");
    }

    // set table and table header attributes
    $datagrid->table_attr = 'align="center" id="dataList" cellpadding="5" cellspacing="0"';
    $datagrid->table_header_attr = 'class="dataListHeader" style="font-weight: bold;"';
    // set delete proccess URL
    $datagrid->chbox_form_URL = $_SERVER['PHP_SELF'];

    // put the result into variables
    $datagrid_result = $datagrid->createDataGrid($dbs, $table_spec, 20, true);
    if (isset($_GET['keywords']) AND $_GET['keywords']) {
        $msg = str_replace('{result->num_rows}', $datagrid->num_rows, __('Found <strong>{result->num_rows}</strong> from your keywords')); //mfc
        echo '<div class="infoBox">'.$msg.' : "'.$_GET['keywords'].'"</div>';
    }

    echo $datagrid_result;
}



