<?php

global $js;

// key to authenticate
define('INDEX_AUTH', '1');

require "../../../ucsysconfig.inc.php";

$node = $dbs->query('SELECT node_id,name,latitude,longitude FROM nodes WHERE (latitude is not null OR longitude is not null)');

$_jsdata = "var locations = [";
$_i = 0;

while ($_point = $node->fetch_array()) {
  $_jsdata .="['".$_point['name']."', ".$_point['latitude'].", ".$_point['longitude'].", ".$_i."],";
  $_i ++;
}
$_jsdata = substr($_jsdata, 0, -1);
$_jsdata .= "];";

?>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxAk6Zm25oNVSm04iOYanZ8D5XEoEIlpE&callback=initMap" type="text/javascript"></script>

  <div id="map" style="width: 100%; height: 474px;"></div>

  <script type="text/javascript">
    function initMap() {
      <?php echo $_jsdata; ?>
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: new google.maps.LatLng(-6.179, 106.83),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });

      var infowindow = new google.maps.InfoWindow();

      var marker, i;

      for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map
        });

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent(locations[i][0]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      }
    }
  </script>
