<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
        @import url("https://fonts.googleapis.com/css?family=Montserrat");
        @font-face {
        font-family: "Montserrat";
        font-style: normal;
        font-weight: 400;
        src: local("Montserrat-Regular"), url(https://fonts.gstatic.com/s/montserrat/v7/zhcz-_WihjSQC0oHJ9TCYL3hpw3pgy2gAi-Ip7WPMi0.woff) format("woff");
        }
        body{
    	font-family: "Montserrat", sans-serif;
        }
        .header,.body,.footer{
        padding: 20px;
        }
        .footer{
        color: white;
        background-color: #0091d6;
        line-height: 1.2;
        font-size:10pt;
        }
        h2{
        color: #0091d6;
        font-weight: normal;
        }
        .body{
        font-size: 11pt;
        background-color: #e1e1e166;
        font-family: arial;
        }
        h3{
        font-weight: lighter;
        }
        .header{
        line-height: 0.6;
        }
        hr{
        border-width: 1px;
        color: #fff3;
        }
        .left{
        float: left;
        }
        .right{
        float: right;
        }
    	.title-header{
    	padding-left: 100px;
    	}
    	.img-header{
    	height: 80px;
    	}
    	.link{
    	text-decoration: none;
    	color:white;
    	}
    	.link:hover{
    	text-decoration: underline;
    	}
    </style>
</head>

<body>
    <div class="header">
    <img  class="img-header left" src="https://psbsekolah.kemdikbud.go.id/kamaya/themes/kamaya/img/logo.png"/>
        <div class="title-header">
            <h2>KAMAYA</h2>
            <h3>Kementerian Pendidikan dan Kebudayaan RI</h3></div>
    </div>
    <div class="body">
    <?php echo $content; ?>
    </div>
    <div class="footer">
        <h4>Pengelola KAMAYA </h4> Perpustakaan Kemendikbud, Gedung A Lantai 1 dan Mezanin
        <br> Jl. Jenderal Sudirman, Senayan – Jakarta
        <br> Indonesia, 10270
        <br>
        <br> Telp. +6221 5707870
        <br> Fax. +6221 5731228
        <br> Email: kamaya@kemdikbud.go.id
        <hr>
        <p>Kamaya © 2017 Perpustakaan Kemdikbud<br/>
        Dikembangkan oleh Pustekkom & <a class="link" href="http://slims.web.id/" target="_BLANK">Senayan Development Community</a></p>
    </div>
</body>

</html>
