<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $page_title; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo UCS_WEB_ROOT_DIR.'admin/admin_themes/'.$sysconf['themes'].'/css/bootstrap.min.css'; ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo UCS_WEB_ROOT_DIR.'admin/admin_themes/'.$sysconf['themes'].'/css/printed.css'; ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo UCS_WEB_ROOT_DIR.'admin/admin_themes/'.$sysconf['themes'].'/css/style.css'; ?>" />
	<?php if (isset($css)) { echo $css; } ?>
	<style type="text/css">
		body { 	
			background: #FFFFFF; 
		}
	</style>
	<?php if (isset($js)) { echo $js; } ?>
</head>
<body>
	<div id="pageContent">
		<?php echo $content; ?>
	</div>
</body>
</html>
