<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $page_title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Union Catalog Server, Library, Catalog, Union Catalog">
    <meta name="author" content="SLiMS, Senayan Developer Community">
    <meta name="theme-color" content="#386f5b" />
    <link rel="stylesheet" href="<?php echo ADMIN_WEB_ROOT_DIR.'admin_themes/'.$sysconf['admin_template']['theme'] ?>/css/style.css?<?php echo date('this')?>">
    <link rel="shortcut icon" href="favicon.ico">

    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/updater.js"></script>
    <script type="text/javascript" src="../js/gui.js"></script>
    <script type="text/javascript" src="../js/form.js"></script>
    <script type="text/javascript" src="../js/calendar.js"></script>
    <script type="text/javascript" src="../js/tiny_mce/tiny_mce.js"></script>
</head>
<body>
    <header class="u-header">
        <div class="u-header__brand container-fluid">
            <div class="row">
                <div class="col">
                    <a href="#" class="u-header__title"><?php echo $sysconf['library_name'] ?></a>
                </div>
                <div class="col">
                    <div class="u-header__user text-right">
                        <a href="./modules/system/app_user.php?changecurrent=true&action=detail" class="subMenuItem"><?php echo __('You are logged as') . ' ' . $_SESSION['realname'] ?></a>
                    </div>
                </div>
            </div>
        </div>
        <nav class="u-header__nav text-center">
            <?php echo strtolower($main_menu) ?>
        </nav>
    </header>
    <main class="u-main">
        <nav class="u-main__menu" id="sidepan">
            <?php echo $sub_menu; ?>
        </nav>
        <section class="u-main__contents"> 
            <div id="mainContent">
                <?php echo $main_content; ?>
            </div>
        </section>
    </main>

    <div id="footer"><?php echo $sysconf['page_footer']; ?></div>

    <!-- fake submit iframe for search form, DONT REMOVE THIS! -->
    <iframe name="blindSubmit" style="display: none; visibility: hidden; width: 0; height: 0;"></iframe>
    <!-- <iframe name="blindSubmit" style="visibility: visible; width: 100%; height: 300px;"></iframe> -->
    <!-- fake submit iframe -->

</body>
</html>